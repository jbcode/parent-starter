# parent-starter

#### 介绍
该项目的详细配套文档见[项目代码说明](https://user.qzone.qq.com/52382868/blog/1587194712)。

在Eclipse中可以很方便的在maven项目右键创建基于他的子项目（在IDEA中只有一个项目，子项目对应module），parent的依赖通过UI界面默认值给出。IDEA这方面的友好性差，需要自己手动改pom.xml代码，不像Eclipse通过带有默认值的界面只需点击Next按钮即可。

下面在IDEA创建一个新的项目——父项目parent-starter，以及它的common-starter子module，以及依赖common的api-service服务module。项目详情请看 [https://user.qzone.qq.com/52382868/blog/1587194712](https://user.qzone.qq.com/52382868/blog/1587194712)

#### 软件架构

软件架构说明
1、 创建父项目parent-starter，springboot项目选择“Spring Initializr”
图片
点击“Next”，填写Group、Artifact信息。注意Type选择“Maven POM”，因为这个项目不需要打jar或者war，只是被依赖的父级项目。
图片

下一页面“dependency”什么都不选择，在具体的子module里面在加入依赖。

下一页面选择项目的存放路径
图片

需要特别注意的一点：
parent-starter项目的pom要去掉<build>......</build>，一定不加springboot的编译插件，相当于使用默认的maven打包插件。否则maven打包时，下面的非springboot项目也执行repackage导致jar不能被依赖，只有真正的springboot子项目才配这个springboot编译插件，这样打出可执行springboot jar。非springboot项目（比如下面的common-starter）也去掉springboot的build设置，打出默认的jar包供其他项目依赖。

由于parent-starter没有业务代码，可以是普通的maven工程。最便捷的创建方式是新建maven项目而不是springboot项目。
图片

2. 配置maven
项目创建成功后，会自动打开右侧边栏的Maven窗口。如果找不到Maven的窗口，可以通过右键点击pom.xml文件-->Maven-->Add Maven Projects来让IDEA识别这个maven项目。另外一种是快捷键“Ctrl+Shift+A”打开Action的操作界面，输入maven，找到Add Maven Projects。

图片

设置自己的setting.xml文件，其中配置了maven资源库镜像地址从默认的maven官网改成了阿里云，来加快依赖包的下载速度 。
图片

3. 新建名称为common-starter的module，提供其他项目依赖的基础功能
右键点击项目“parent-starter”-->new -->module，还是选择“Spring Initializr”，填入必要信息，注意type不是“Maven POM”选择常用的“Maven Project”
图片

目录结构
图片

由于common项目不需要打包成springboot的jar或者war，只是被其他项目依赖的普通的maven项目jar，所以删掉pom.xml最下面springboot打包插件配置，这一步非常重要，否则依赖他的项目会找不到类，如果非要common项目既可以打包成springboot单独运行又可以被依赖，可以参考我前面关于springboot打包的文章。
<build>
<plugins>
<plugin>
<groupId>org.springframework.boot</groupId>
<artifactId>spring-boot-maven-plugin</artifactId>
</plugin>
</plugins>
</build>

4、配置项目的父子依赖关系并使用maven打包
修改parent-starter项目pom.xml的packaging为pom类型，只有pom类型才能作为其他项目的parent
图片

修改common-starter项目的pom.xml的parent依赖为parent-starter
图片

在父项目的pom文件添加module配置来管理子module
图片

有了父子依赖关系就可以在parent-starter项目进行打包（按Ctrl选择clean和install），会把下面的所有module都自动打包。注意：不要在子module进行maven打包，因为子module依赖的其他module可能还没打包，而找不到依赖报错。
为了加快速度可以把maven窗口的闪电图标（点击下图的>>展示隐藏图标）打开，忽略运行测试（Skip tests）。
图片

如果没有报错会看到成功install jar文件到D:\dev\mvnRespo资源仓库的信息：
[INFO] Installing D:\dev\workspace-IDEA\infra-project\parent-starter\common-starter\target\common-starter-0.0.1-SNAPSHOT.jar to D:\dev\mvnRespo\com\boot\parent\common-starter\0.0.1-SNAPSHOT\common-starter-0.0.1-SNAPSHOT.jar
[INFO] Installing D:\dev\workspace-IDEA\infra-project\parent-starter\common-starter\pom.xml to D:\dev\mvnRespo\com\boot\parent\common-starter\0.0.1-SNAPSHOT\common-starter-0.0.1-SNAPSHOT.pom
[INFO] ------------------------------------------------------------------------
[INFO] parent-starter ..................................... SUCCESS [  1.526 s]
[INFO] common-starter ..................................... SUCCESS [  4.016 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  5.844 s

5、编写common的工具类
public class DateUtil {
/**
* 返回当前时间的yyyy-MM-dd HH:mm:ss字符串格式。
*
* @return yyyy-MM-dd HH:mm:ss
*/
public static String getStringDate() {
Date currentTime = new Date();
SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
String dateString = formatter.format(currentTime);
return dateString;
}
}

6、 新建springboot的服务module
使用parent-starter作为parent，common作为依赖，提供Restful API服务接口。
跟common-starter项目类似步骤新建module，右键点击项目“parent-starter”-->new -->module，还是选择“Spring Initializr”，填入必要信息，新module的Artifact为api-service，注意type不是“Maven POM”选择常用的“Maven Project”。

图片

点击下一步，由于要提供Restful接口，依赖页面选择web-->spring web
图片

在父项目pom.xml通过配置module来添加“api-service”
<modules>
<module>common-starter</module>
<module>api-service</module>
</modules>

在api-service项目增加对common-starter的依赖
<dependency>
<groupId>com.boot.parent</groupId>
<artifactId>common-starter</artifactId>
<version>0.0.1-SNAPSHOT</version>
</dependency>

新增RestController类，调用了common-starter的类。
@RestController
public class GuoController {
@GetMapping("/time/get")
public String getCurrentTime(@RequestParam String name) {
return "用户：" + name + ",请求当前时间：" + DateUtil.getStringDate();
}
}

目录结构
图片


启动api-service项目，通过访问http://localhost:8080/time/get?name=郭秀志， 返回这个人询问的时间
图片

7、上传代码到Gitee
先创建本地git 仓库
图片

点击工具类的“对号”图标提交代码到本地Git
图片

提交代码到远程Gitee
图片

提交后可以看到成功信息：
To https://gitee.com/jbcode/parent-starter.git
*refs/heads/master:refs/heads/master[new branch]
Branch 'master' set up to track remote branch 'master' from 'origin'.
Done

该项目源代码已经上传到公开的Gitee仓库，可以自由下载，登录到Gitee控制台可以看到项目的目录结构，地址为：https://gitee.com/jbcode/parent-starter

8、配置数据库连接
新增application.yml文件，配置druid数据源连接。详见gitee代码

9、通过数据库生产entity

首先，建立数据库连接
 打开Database窗口配置数据库连接，点击“+”新建一个数据库连接
图片

在新建数据源的页面，填完信息后有个“Test”按钮可以测试是否连接正常，会提示设置server time zone，如下在“Advance”标签页设置“UTC”后保存，再测试连接正常。
图片

其次，项目添加JPA feature
通过快捷键“Ctrl+Shift+Alt+s”或者File菜单打开“Project Structure”，给api-service项目添加JPA。
图片

然后，pom文件添加JPA、Druid、mysql依赖
<dependency>
<groupId>org.springframework.boot</groupId>
<artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
<groupId>com.alibaba</groupId>
<artifactId>druid</artifactId>
<version>1.1.19</version>
</dependency>
 <dependency>
<groupId>mysql</groupId>
<artifactId>mysql-connector-java</artifactId>
<scope>runtime</scope>
</dependency>
接下来，打开“persistence”窗口生产entity类
右键项目名称“api-service”-->Generate Persistence Mapping-->By Database Schema
图片

生成的Entity类如下
图片

上图的红框错误，通过alt+enter快捷键，进行指定数据源来修复
图片

10、新建repository类操作数据库
package com.boot.backend.apiservice.repository;

import com.boot.backend.apiservice.entity.BzRtInterfaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
* @ClassName: WhiteListPayerRepository
* @Description:持久层类，定义跟数据库操作的接口
* @author: 郭秀志 jbcode@126.com
* @date: 2020年4月19日 上午10:43:35
* @Copyright:
*/
public interface BzRtInterfaceRepository extends JpaRepository<BzRtInterfaceEntity, Long> {

}


11、新建service接口和实现类来调用Repository类
package com.boot.backend.apiservice.service;

/**
* @ClassName: IWhiteListPayerService
* @Description: 
* @author: 郭秀志 jbcode@126.com
* @date: 2020/4/19 14:48 
* @Copyright: 
*/
public interface IBzRtInterfaceService {

}
-------分割线---------
package com.boot.backend.apiservice.service.impl;

import com.boot.backend.apiservice.entity.BzRtInterfaceEntity;
import com.boot.backend.apiservice.repository.BzRtInterfaceRepository;
import com.boot.backend.apiservice.service.IBzRtInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
* @ClassName: BzRtInterfaceServiceImpl
* @Description:
* @author: 郭秀志 jbcode@126.com
* @date: 2020/4/19 14:52
* @Copyright:
*/
@Service
public class BzRtInterfaceServiceImpl implements IBzRtInterfaceService {

@Autowired
private BzRtInterfaceRepository interfaceRepository;

public List<BzRtInterfaceEntity> findAll() {
return interfaceRepository.findAll();
}
}

12、新建DBController来操作数据库
package com.boot.backend.apiservice.controller;

import com.boot.backend.apiservice.entity.BzRtInterfaceEntity;
import com.boot.backend.apiservice.service.IBzRtInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
* @ClassName: DbController
* @Description:
* @author: 郭秀志 jbcode@126.com
* @date: 2020/4/19 14:56
* @Copyright:
*/
@RestController
public class DbController {

@Autowired
private IBzRtInterfaceService rtInterfaceService;

@GetMapping("/getAll")
public List<BzRtInterfaceEntity> getAllDataFromDb() {
return rtInterfaceService.findAll();
}
}

启动后访问接口/getAll，可以看到数据打印成Json格式。
图片



#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
