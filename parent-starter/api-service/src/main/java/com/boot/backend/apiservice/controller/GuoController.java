package com.boot.backend.apiservice.controller;

import com.boot.parent.commonstarter.util.DateUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: GuoController
 * @Description: 通过访问http://localhost:8080/time/get?name=gxz 返回这个人询问的时间
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/4/18 14:29
 * @Copyright:
 */
@RestController
public class GuoController {
    @GetMapping("/time/get")
    public String getCurrentTime(@RequestParam String name) {
        return "用户：" + name + ",请求当前时间：" + DateUtil.getStringDate();
    }
}
