package com.boot.backend.apiservice.entity;

import java.util.Objects;

/**
 * @ClassName: BzRtInterfaceEntity
 * @Description:
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/4/19 14:31
 * @Copyright:
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "bz_rt_interface", schema = "bankrouter")
@javax.persistence.IdClass(BzRtInterfaceEntityPK.class)
public class BzRtInterfaceEntity {
    private String id;
    private String inerfaceName;
    private String bankId;
    private String groupId;
    private String inerfaceStype;
    private String inerfaceCode;

    @javax.persistence.Id
    @javax.persistence.Column(name = "id", nullable = false, length = 30)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @javax.persistence.Id
    @javax.persistence.Column(name = "inerface_name", nullable = false, length = 30)
    public String getInerfaceName() {
        return inerfaceName;
    }

    public void setInerfaceName(String inerfaceName) {
        this.inerfaceName = inerfaceName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "bank_id", nullable = false, length = 30)
    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "group_id", nullable = false, length = 30)
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "inerface_stype", nullable = false, length = 2)
    public String getInerfaceStype() {
        return inerfaceStype;
    }

    public void setInerfaceStype(String inerfaceStype) {
        this.inerfaceStype = inerfaceStype;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "inerface_code", nullable = false, length = 100)
    public String getInerfaceCode() {
        return inerfaceCode;
    }

    public void setInerfaceCode(String inerfaceCode) {
        this.inerfaceCode = inerfaceCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BzRtInterfaceEntity that = (BzRtInterfaceEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(inerfaceName, that.inerfaceName) &&
                Objects.equals(bankId, that.bankId) &&
                Objects.equals(groupId, that.groupId) &&
                Objects.equals(inerfaceStype, that.inerfaceStype) &&
                Objects.equals(inerfaceCode, that.inerfaceCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, inerfaceName, bankId, groupId, inerfaceStype, inerfaceCode);
    }
}
