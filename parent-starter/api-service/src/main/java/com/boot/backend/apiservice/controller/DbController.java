package com.boot.backend.apiservice.controller;

import com.boot.backend.apiservice.entity.BzRtInterfaceEntity;
import com.boot.backend.apiservice.service.IBzRtInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName: DbController
 * @Description:
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/4/19 14:56
 * @Copyright:
 */
@RestController
public class DbController {

    @Autowired
    private IBzRtInterfaceService rtInterfaceService;

    @GetMapping("/getAll")
    public List<BzRtInterfaceEntity> getAllDataFromDb() {
        return rtInterfaceService.findAll();
    }
}
