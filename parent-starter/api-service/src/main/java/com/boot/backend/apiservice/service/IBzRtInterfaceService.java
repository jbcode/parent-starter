package com.boot.backend.apiservice.service;

import com.boot.backend.apiservice.entity.BzRtInterfaceEntity;

import java.util.List;

/**
 * @ClassName: IWhiteListPayerService
 * @Description:
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/4/19 14:48
 * @Copyright:
 */
public interface IBzRtInterfaceService {
    public List<BzRtInterfaceEntity> findAll();
}
