package com.boot.backend.apiservice.service.impl;

import com.boot.backend.apiservice.entity.BzRtInterfaceEntity;
import com.boot.backend.apiservice.repository.BzRtInterfaceRepository;
import com.boot.backend.apiservice.service.IBzRtInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: BzRtInterfaceServiceImpl
 * @Description:
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/4/19 14:52
 * @Copyright:
 */
@Service
public class BzRtInterfaceServiceImpl implements IBzRtInterfaceService {

    @Autowired
    private BzRtInterfaceRepository interfaceRepository;

    @Override
    public List<BzRtInterfaceEntity> findAll() {
        return interfaceRepository.findAll();
    }
}
