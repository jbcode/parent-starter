package com.boot.backend.apiservice.repository;

import com.boot.backend.apiservice.entity.BzRtInterfaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName: WhiteListPayerRepository
 * @Description:持久层类，定义跟数据库操作的接口
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020年4月19日 上午10:43:35
 * @Copyright:
 */
public interface BzRtInterfaceRepository extends JpaRepository<BzRtInterfaceEntity, Long> {

}
