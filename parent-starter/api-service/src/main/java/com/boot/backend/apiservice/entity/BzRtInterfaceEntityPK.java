package com.boot.backend.apiservice.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * @ClassName: BzRtInterfaceEntityPK
 * @Description:
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/4/19 14:31
 * @Copyright:
 */
public class BzRtInterfaceEntityPK implements Serializable {
    private String id;
    private String inerfaceName;

    @javax.persistence.Column(name = "id", nullable = false, length = 30)
    @javax.persistence.Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @javax.persistence.Column(name = "inerface_name", nullable = false, length = 30)
    @javax.persistence.Id
    public String getInerfaceName() {
        return inerfaceName;
    }

    public void setInerfaceName(String inerfaceName) {
        this.inerfaceName = inerfaceName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BzRtInterfaceEntityPK that = (BzRtInterfaceEntityPK) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(inerfaceName, that.inerfaceName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, inerfaceName);
    }
}
