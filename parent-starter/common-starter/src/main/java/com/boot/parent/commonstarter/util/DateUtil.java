package com.boot.parent.commonstarter.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName: DateUtil
 * @Description: 时间方面的工具类
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/4/18 13:59
 * @Copyright:
 */
public class DateUtil {
    /**
     * 返回当前时间的yyyy-MM-dd HH:mm:ss字符串格式。
     *
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        return dateString;
    }
}
